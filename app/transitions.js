export default function(){
  this.transition(
    this.fromRoute('auth.login'),
    this.toRoute('auth.signup'),
    this.use('fade'),
    this.reverse('fade')
  );
}
