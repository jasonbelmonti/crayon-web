import { computed } from '@ember/object';
import { validatePassword, validateEmail } from 'crayon-web/utils/validation/validators';
import LoginForm from 'crayon-web/components/auth/login-form';

export default LoginForm.extend({
  passwordVerification: undefined,
  validationSpec: [
    {
      property: 'email',
      validators: [
        {
          test: function(email) {
            return email !== '' && email !== undefined;
          },
          error: 'enter your email'
        },
        {
          test: function(email) {
            return validateEmail(email);
          },
          error: 'enter a valid email'
        }
      ]
    },
    {
      property: 'password',
      validators: [
        {
          test: function(password) {
            return password !== '' && password !== undefined;
          },
          error: 'enter your password'
        },
        {
          test: function(password) {
            return validatePassword(password);
          },
          error: 'password is too weak'
        },
        {
          test: function(password, component) {
            return password === component.get('passwordVerification');
          },
          error: 'passwords do not match'
        }
      ]
    }
  ],
  isSubmitEnabled: computed('email', 'password', 'passwordVerification', function() {
    return this.get('email') && this.get('password') && this.get('passwordVerification');
  })
});