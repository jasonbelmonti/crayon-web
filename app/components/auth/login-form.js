import Component from '@ember/component';
import { computed } from '@ember/object';
import  PropertiesValidator from 'crayon-web/mixins/properties-validator';
import { validateEmail, validatePassword } from 'crayon-web/utils/validation/validators';

export default Component.extend(PropertiesValidator, {
  // these should be passed at invocation time
  classNames: ['container', 'auth-form-controls', 'opaque'],
  email: undefined,
  password: undefined,
  validationErrors: undefined,
  validationSpec: [
    {
      property: 'email',
      validators: [
        {
          test: function(email) {
            return email !== '' && email !== undefined;
          },
          error: 'enter your email'
        },
        {
          test: function(email) {
            return validateEmail(email);
          },
          error: 'enter a valid email'
        }
      ]
    },
    {
      property: 'password',
      validators: [
        {
          test: function(password) {
            return password !== '' && password !== undefined;
          },
          error: 'enter your password'
        },
        {
          test: function(password) {
            return validatePassword(password);
          },
          error: 'enter a valid password'
        }
      ]
    }
  ],

  // should this be more universal, checking an array of required props?
  isSubmitEnabled: computed('email', 'password', function() {
    return this.get('email') && this.get('password');
  }),
  actions: {
    submit() {
      this.set('validationErrors', undefined);

      const validationErrors = this.validateProps();

      if(validationErrors.length > 0) {
        this.set('validationErrors', validationErrors);
      } else {
        this.get('onSubmit')(this.get('email'), this.get('password'));
      }
    }
  },
  didInsertElement() {
    // setting initial focus should be optional
    // the focused element should be parameterized
    this.$('[name="email"]').focus();
  }
});