import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  firebase: service(),

  actions: {
    verifyEmail() {
      this.set('isLoading', true);
      this.get('firebase').sendVerificationEmail(
      () => {
        this.set('isLoading', false);
        this.get('onVerificationEmailSent')()
      },
      (/*error*/) => {
        this.set('isLoading', false);
      });
    }
  }
});