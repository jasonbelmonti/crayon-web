import ENV from 'crayon-web/config/environment';

export function logMessage(message, level, prefix, data) {
  message = `[${prefix}]: ${message}`;
  if(data && ENV.environment === 'development') {
    // eslint-disable-next-line no-console
    console.log('[log-helpers:DATA]', data);
  }

  Ember[level](message);
}