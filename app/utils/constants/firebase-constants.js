export const COLLECTIONS = {
  ACCOUNTS: 'accounts'
};

export const DOCUMENT_FIELDS = {
  ACCOUNT: {
    OWNER: "owner_uuid"
  }
};
