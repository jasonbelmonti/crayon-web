/**
 * See https://firebase.google.com/docs/reference/js/firebase.auth.Error
 * @module  constants/AuthErrors
 */

export const AUTH_ERRORS = {
  // Thrown if the instance of FirebaseApp has been deleted.
  'auth/app-deleted': 'there was a problem authenticating',

  // Thrown if the app identified by the domain where it's hosted, is not authorized to use Firebase Authentication with the provided API key. Review your key configuration in the Google API console.
  'auth/app-not-authorized': 'there was a problem authenticating',

  // Thrown if a method is called with incorrect arguments.
  'auth/argument-error': 'there was a problem authenticating',

  // Thrown if the provided API key is invalid. Please check that you have copied it correctly from the Firebase Console.
  'auth/invalid-api-key': 'there was a problem authenticating',

  // Thrown if the user's credential is no longer valid. The user must sign in again.
  'auth/invalid-user-token': 'your session has expired',

  // Thrown if a network error (such as timeout, interrupted connection or unreachable host) has occurred.
  'auth/network-request-failed': 'a network issue interfered with authentication',

  // Thrown if you have not enabled the provider in the Firebase Console. Go to the Firebase Console for your project, in the Auth section and the Sign in Method tab and configure the provider.
  'auth/operation-not-allowed': 'provider not allowed',

  // Thrown if the user's last sign-in time does not meet the security threshold. Use firebase.User#reauthenticateWithCredential to resolve. This does not apply if the user is anonymous.
  'auth/requires-recent-login': 'your session has expired',

  // Thrown if requests are blocked from a device due to unusual activity. Trying again after some delay would unblock.
  'auth/too-many-requests': 'suspicous activity detected',

  // Thrown if the app domain is not authorized for OAuth operations for your Firebase project. Edit the list of authorized domains from the Firebase console.
  'auth/unauthorized-domain': 'your session has expired',

  // Thrown if the user account has been disabled by an administrator. Accounts can be enabled or disabled in the Firebase Console, the Auth section and Users subsection.
  'auth/user-disabled': 'this account is suspended',

  // Thrown if the user's credential has expired. This could also be thrown if a user has been deleted. Prompting the user to sign in again should resolve this for either case.
  'auth/user-token-expired': 'your session has expired',

  // Thrown if the browser does not support web storage or if the user disables them.
  'auth/web-storage-unsupported': 'your browser is not compatible with crayon',

  // Thrown if a user with this email already exists
  'auth/email-already-in-use': 'this account already exists',

  'auth/user-not-found': 'this account doesn\'t exist'
};