import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  firebase: service(),

  actions: {
    onVerificationEmailSent() {
      this.send('logOut', 'verificationEmailSent');
    }
  }
});
