import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    onSubmit(email, password) {
      this.set('isLoading', true);
      // this._signupNewUser(email, password);
      this.send('signupNewUser', email, password);
    }
  }
});
