import Controller from '@ember/controller';
import { AUTH_ERRORS } from 'crayon-web/utils/constants/firebase/auth-errors'
import { computed } from '@ember/object';


export default Controller.extend({
  queryParams: {
    code: undefined
  },

  errorMessage: computed('code', function() {
    return AUTH_ERRORS[this.get('code')];
  })
});
