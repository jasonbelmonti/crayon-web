import Controller from '@ember/controller';
import { AUTH_ERRORS } from 'crayon-web/utils/constants/firebase/auth-errors';

export default Controller.extend({
  AUTH_ERRORS: AUTH_ERRORS,
  queryParams: {
    message: undefined,
    error: undefined
  },
  actions: {
    onSubmit(email, password) {
      this.set('message', undefined);
      this.set('error', undefined);
      this.send('loginUser', email, password);
    }
  }
});
