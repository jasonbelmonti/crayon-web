import config from '../config/environment';

export function initialize(/* application */) {
  firebase.initializeApp(config.firebase);
  firebase.firestoreDB = firebase.firestore();
}

export default {
  initialize
};
