import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  firebase: service(),

  actions: {
    logOut(message) {
      this.get('firebase').logout().then(() => {
        if(message) {
          const queryParams = {
            queryParams: {
              message
            }
          };

          this.transitionTo('auth.login', queryParams);
        } else {
          this.transitionTo('auth.login');
        }

      });
    }
  },

  model() {
    const lookupPromise = new Ember.RSVP.Promise((resolve, reject) => {
      const firebase = this.get('firebase');
      firebase.getCurrentUserAccounts()
        .then((accounts) => {
          if(accounts.length > 0) {
            resolve(accounts);
          } else {
            firebase.createUserAccount().then(function(uid) {
              debugger
              resolve(uid);
            })
          }
        })
        .catch((error) => {
          reject(error);
        })
    });

    return lookupPromise
  }
});
