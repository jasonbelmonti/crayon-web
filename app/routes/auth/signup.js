import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  firebase: service(),

  actions: {
    signupNewUser(email, password) {
      this._signupNewUser(email, password);
    }
  },

  _signupNewUser(email, password) {
    this.get('firebase').createUser(email, password,
    (user) => {
      Ember.debug(`[AUTH] signup success! ${user}`);
      this.set('isLoading', false);
      this.transitionTo('auth.login', { queryParams: { message: 'signupSuccessful' } });
    },
    ({ code }) => {
      this.transitionTo('auth.error', { queryParams: { code: code } });
    });
  }
});
