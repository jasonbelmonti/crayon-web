import Route from '@ember/routing/route';
import { logMessage } from 'crayon-web/utils/logging/log-helpers';
import { inject as service } from '@ember/service';

export default Route.extend({

  firebase: service(),

  actions: {
    loginUser(email, password) {
      this._loginUser(email, password);
    }
  },

  _loginUser(email, password) {
    this.controller.set('isLoading', true);
    this._loginWithFirebase(email, password)
      .then(this._onLoginSuccess.bind(this))
      .catch(this._onLoginError.bind(this));
  },

  _loginWithFirebase(email, password) {
    return this.get('firebase').login(email, password);
  },

  _onLoginSuccess() {
    this.transitionTo('configuration.home');
  },

  _onLoginError(error) {
    logMessage('An error occurred while attempting to log in', 'debug', 'AUTH', error);
    this.transitionTo('auth.login', { queryParams: { error: error.code } });
  },

  setupController(controller) {
    controller.set('isLoading', false);
  },

  resetController(controller) {
    controller.set('message', undefined);
    controller.set('error', undefined);
  }
});
