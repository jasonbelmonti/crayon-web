import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { logMessage } from 'crayon-web/utils/logging/log-helpers';
// all calls to firebase should be in the service

export default Route.extend({

  firebase: service(),

  beforeModel(transition) {
    const { targetName } = transition;

    // we need to redirect already authenticated users home
    if(targetName.indexOf('auth') === -1) {
      // why do we need to return here?
      // what happens on error?
      return this._fetchSession(transition)
      .then(() => {
        if (targetName === 'configuration.index') {
          this.transitionTo('configuration.home');
        }
      })
      .catch(() => {
        logMessage('user is not logged in', 'warn', 'AUTH');
        this.transitionTo('auth.login');
      });
    }
  },

  _fetchSession() {
     return new Ember.RSVP.Promise((resolve, reject) => {
      let auth = this.get('firebase.auth');
      const unsub = auth.onAuthStateChanged((user) => {
        unsub();
        this._bindTokenObserver();
        resolve(user);
      },
      (err) => {
        unsub();
        reject(err);
      });
    });
  },

  _bindTokenObserver() {
    this.get('firebase').bindTokenObserver((user) => {
      if (!user) {
        this._onUserUnauthenticated(user);
      }
    });
  },

  _onUserUnauthenticated() {
    Ember.run(() => {
      this.transitionTo('auth.login')
    });
  }
});