import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('configuration', { path: '/', resetNamespace: true }, function() {

    // authentication
    this.route('auth', { resetNamespace: true }, function() {
      this.route('login');
      this.route('signup');
      this.route('error');
    });

    this.route('home');
  });
});

export default Router;
