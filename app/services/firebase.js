import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import ENV from 'crayon-web/config/environment';
import {
  COLLECTIONS,
  DOCUMENT_FIELDS
} from 'crayon-web/utils/constants/firebase-constants';

export default Service.extend({
  firebase: firebase,

  ajax: service('crayon-ajax'),

  auth: computed(function() {
    return this.get('firebase').auth();
  }),

  currentUser: computed(function() {
    return this.get('auth').currentUser;
  }),

  db: computed(function() {
    return this.get('firebase.firestoreDB');
  }),

  accounts: computed(function() {
    return this.get('db').collection(COLLECTIONS.ACCOUNTS);
  }),

  sessionToken: undefined,

  userToken() {
    const tokenPromise = new Ember.RSVP.Promise((resolve, reject) => {
      this.get('currentUser').getIdToken(true)
        .then((token) => {
          resolve(token);
        })
        .catch((error) => {
          reject(error);
        })
    });

    return tokenPromise;
  },

  login(email, password) {
    return this.get('auth').signInWithEmailAndPassword(email, password);
  },

  logout() {
    return this.get('auth').signOut();
  },

  bindTokenObserver(observer) {
    this.get('auth').onIdTokenChanged(observer);
  },

  sendVerificationEmail(success, fail) {
    this.get('auth').currentUser.sendEmailVerification()
      .then(success)
      .catch(fail);
  },

  createUser(email, password, success, fail) {
    this.get('auth').createUserWithEmailAndPassword(email, password)
      .then(success)
      .catch(fail);
  },

  getCurrentUserAccounts() {
    return new Ember.RSVP.Promise((resolve, reject) => {
      const currentUserId = this.get('currentUser').uid;
      this.get('accounts').where(DOCUMENT_FIELDS.ACCOUNT.OWNER, '==', currentUserId).get()
      .then(function({ docs }) {
        resolve(docs.map((doc) => doc.data()));
      })
      .catch(function(error) {
        reject(error);
      });
    });
  },

  createUserAccount() {
    return new Ember.RSVP.Promise((resolve, reject) => {
      const currentUserId = this.get('currentUser').uid;
      this._crayonAPI('createUserAccount', 'POST').then((result) => {
        console.log(result);
      });
    });
  },

  _crayonAPI(endpoint, method, data) {
    const apiPromise = new Ember.RSVP.Promise((resolve, reject) => {
      this.userToken()
        .then((token) => {
          this.set('sessionToken', token);
          const requestOptions = {
            method,
            data,
            headers: {
              authorization: `Bearer ${token}`
            }
          };
          this.get('ajax').request(this._crayonAPIURL(endpoint), requestOptions)
            .then((reqResult) => {
              resolve(reqResult);
            })
            .catch((reqError) => {
              reject(reqError);
            })
        })
        .catch((tokenError) => {
          reject(tokenError);
        });
    });

    return apiPromise;
  },

  _crayonAPIURL(endpoint) {
    return `${ENV.SERVICES.CRAYON_API.LOCATION}${endpoint}`;
  },
});