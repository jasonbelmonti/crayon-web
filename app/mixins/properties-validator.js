import Mixin from '@ember/object/mixin';

export default Mixin.create({
  validateProps() {
    const spec = this.get('validationSpec');
    const errors = [];

    spec.forEach(({ property:propertyName, validators }) => {
      const propertyValue = this.get(propertyName);
      for(let i=0; i < validators.length; i++) {
        const { test, error } = validators[i];
        if(!test(propertyValue, this)) {
          errors.push(error);
          break;
        }
      }
    });

    return errors;
  }
});
