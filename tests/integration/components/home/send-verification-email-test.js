import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('home/send-verification-email', 'Integration | Component | home/send verification email', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{home/send-verification-email}}`);

  assert.equal(
    this.$().text().trim().replace(/\s/g, ""),
    'notverifiedemailsendverificationemail'
  );
});
