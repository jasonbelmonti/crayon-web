import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import sinon from 'sinon';

moduleForComponent('auth/login-form', 'Integration | Component | auth/login form', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{auth/login-form}}`);

  assert.equal(this.$().text().trim(), 'login');
});

test('the initial state of the login button is disabled', function(assert) {
  this.render(hbs`{{auth/login-form}}`);

  assert.equal(this.$('button').attr('disabled'), 'disabled');
});

test('the button is enabled when password and email fields are not blank', function(assert) {
  this.render(hbs`{{auth/login-form}}`);
  const $emailInput = this.$('[name="email"]');
  const $passwordInput = this.$('[name="password"]');
  const $loginButton = this.$('button');

  assert.equal($loginButton.attr('disabled'), 'disabled');

  // enter values in both fields
  $emailInput.val('fart');
  $emailInput.change();
  $passwordInput.val('fart');
  $passwordInput.change();

  assert.equal(
    $loginButton.attr('disabled'),
    undefined,
    'the button is enabled when both fields are not empty'
  );

  // empty the email field
  $emailInput.val('');
  $emailInput.change();

  assert.equal(
    $loginButton.attr('disabled'),
    'disabled',
    'the button is disabled for an empty email'
  );

  // add a value back to the email field
  $emailInput.val('fart');
  $emailInput.change();

  // empty the password field
  $passwordInput.val('');
  $passwordInput.change();

  assert.equal(
    $loginButton.attr('disabled'),
    'disabled',
    'the button is disabled for an empty password'
  );
});

test('errors are displayed when fields are invalid', function(assert) {
  const submitStub = sinon.stub();
  this.set('onSubmit', submitStub);
  this.render(hbs`{{auth/login-form onSubmit=onSubmit}}`);

  const $emailInput = this.$('[name="email"]');
  const $passwordInput = this.$('[name="password"]');
  const $loginButton = this.$('button');

  // enter values in both fields
  $emailInput.val('fart');
  $emailInput.change();
  $passwordInput.val('fart');
  $passwordInput.change();


  $loginButton.click();
  // TODO: add sinon and assert that the submit method was not invoked
  assert.notOk(
    submitStub.caleld,
    'the passed submit action was invoked as expected'
  );

  assert.ok(
    this.$('.container.errors').length > 0,
    'the error container is visible'
  )
});


test('the passed submit method is invoked when inputs are valid', function(assert) {
  const submitStub = sinon.stub();
  this.set('onSubmit', submitStub);
  this.render(hbs`{{auth/login-form onSubmit=onSubmit}}`);

  const $emailInput = this.$('[name="email"]');
  const $passwordInput = this.$('[name="password"]');
  const $loginButton = this.$('button');

  // enter values in both fields
  $emailInput.val('belmo@fart.com');
  $emailInput.change();
  $passwordInput.val('!TestPassword666');
  $passwordInput.change();


  $loginButton.click();
  // TODO: add sinon and assert that the submit method was not invoked
  assert.ok(
    submitStub.called,
    'the passed submit action was invoked as expected'
  );

  assert.ok(
    this.$('.container.errors').length === 0,
    'the error container is not visible'
  )
});
