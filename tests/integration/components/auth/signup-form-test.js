import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('auth/signup-form', 'Integration | Component | auth/signup form', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{auth/signup-form}}`);

  assert.equal(this.$().text().trim(), 'sign up');
});

test('the initial state of the sign up button is disabled', function(assert) {
  this.render(hbs`{{auth/signup-form}}`);

  assert.equal(this.$('button').attr('disabled'), 'disabled');
});

test('the button is enabled when password and email fields are not blank', function(assert) {
  this.render(hbs`{{auth/signup-form}}`);
  const $emailInput = this.$('[name="email"]');
  const $passwordInput = this.$('[name="password"]');
  const $passwordVerificationInput = this.$('[name="password-verification"]');
  const $signupButton = this.$('button');

  assert.equal($signupButton.attr('disabled'), 'disabled');

  // enter values in both fields
  $emailInput.val('fart');
  $emailInput.change();
  $passwordInput.val('fart');
  $passwordInput.change();

  assert.equal(
    $signupButton.attr('disabled'),
    'disabled',
    'the button is disabled for empty password verification'
  );

  $passwordVerificationInput.val('fart');
  $passwordVerificationInput.change();

  assert.equal(
    $signupButton.attr('disabled'),
    undefined,
    'the button is disabled when password verification is empty'
  );

  // empty the email field
  $emailInput.val('');
  $emailInput.change();

  assert.equal(
    $signupButton.attr('disabled'),
    'disabled',
    'the button is disabled for an empty email'
  );

  // add a value back to the email field
  $emailInput.val('fart');
  $emailInput.change();

  // empty the password field
  $passwordInput.val('');
  $passwordInput.change();

  assert.equal(
    $signupButton.attr('disabled'),
    'disabled',
    'the button is disabled for an empty password'
  );
});