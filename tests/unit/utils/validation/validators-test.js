import { moduleFor, test } from 'ember-qunit';
import { validateEmail, validatePassword } from 'crayon-web/utils/validation/validators';

moduleFor('util:validation/validators', 'Unit | Utils | validators', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

test('it validates emails correctly', function(assert) {
  assert.notOk(validateEmail('f'));
  assert.notOk(validateEmail('fart'));
  assert.notOk(validateEmail('fart.com'));
  assert.notOk(validateEmail('.'));
  assert.notOk(validateEmail(''));
  assert.notOk(validateEmail(' '));
  assert.notOk(validateEmail('yolo@foo'));
  assert.notOk(validateEmail('dork.foo'));
  assert.notOk(validateEmail('$$'));
  assert.notOk(validateEmail('dork$foo'));
  assert.ok(validateEmail('jason@belmocorp.com'));
});

test('it validates passwords correctly', function(assert) {
  // sanity check
  assert.notOk(validatePassword('f'));

  assert.notOk(validatePassword('fffffffffff'));
  assert.notOk(validatePassword('fffFfffffff'));
  assert.notOk(validatePassword('fffFffa!'));

  // no special character, no capital letter
  assert.notOk(validatePassword('10002334'));
  assert.notOk(validatePassword('!10002334'));

  // no capital letter
  assert.notOk(validatePassword('!10002334a'));

  // invalid special characters
  assert.notOk(validatePassword('|10002334a'));
  assert.notOk(validatePassword('.10002334a'));

  // valid passwords
  assert.ok(validatePassword('!10002334aB'));
  assert.ok(validatePassword('1QffFffa!'));
  assert.ok(validatePassword('1QFFfFAAAA!'));
});
