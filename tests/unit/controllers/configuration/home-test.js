import { moduleFor, test } from 'ember-qunit';

moduleFor('controller:configuration/home', 'Unit | Controller | configuration/home', {
  // Specify the other units that are required for this test.
  needs: ['service:firebase']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  let controller = this.subject();
  assert.ok(controller);
});
