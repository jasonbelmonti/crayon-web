import EmberObject from '@ember/object';
import PropertiesValidatorMixin from 'crayon-web/mixins/properties-validator';
import { module, test } from 'qunit';

module('Unit | Mixin | properties validator');

// Replace this with your real tests.
test('it works', function(assert) {
  let PropertiesValidatorObject = EmberObject.extend(PropertiesValidatorMixin);
  let subject = PropertiesValidatorObject.create();
  assert.ok(subject);
});
