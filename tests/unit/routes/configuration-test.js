import { moduleFor, test } from 'ember-qunit';

function dummyRequest() {
  return new Ember.RSVP.Promise(() => {});
}


moduleFor('route:configuration', 'Unit | Route | configuration', {
  // Specify the other units that are required for this test.
  needs: ['service:firebase'],

  beforeEach() {
    const firebaseSessionStub = Ember.Service.extend({
      open: dummyRequest,
      fetch: dummyRequest,
      close: dummyRequest
    });

    const firebaseStub = Ember.Service.extend({
      bindAuthStateObserver: sinon.stub()
    });

    this.register('service:firebaseSession', firebaseSessionStub);
    this.inject.service('firebaseSession', { as: 'firebaseSession' });
    this.register('service:firebase', firebaseStub);
    this.inject.service('firebase', { as: 'firebase' });
  }
});

test('it exists', function(assert) {
  let route = this.subject();
  assert.ok(route);
});
