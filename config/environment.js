'use strict';

const DEVELOPMENT_SERVICES = {
  CRAYON_API: {
    LOCATION: 'http://localhost:5000/crayon-94397/us-central1/api/'
  }
};

const PRODUCTION_SERVICES = {
  CRAYON_API: {
    LOCATION: 'https://us-central1-crayon-94397.cloudfunctions.net/api/'
  }
};

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'crayon-web',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {    },

    firebase: {
      apiKey: "AIzaSyANeFH3QaRDELkEYtJXfmvdq7tZc6g9Zlo",
      authDomain: "crayon-94397.firebaseapp.com",
      projectId: "crayon-94397"
    },
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.SERVICES = DEVELOPMENT_SERVICES;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    ENV.SERVICES = PRODUCTION_SERVICES;
  }

  return ENV;
};
